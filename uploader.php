<?php
class GetURL {
	/**
	 * Save the file to the specified path
	 * @return boolean TRUE on success
	 */

	private $file;

	function __construct($url, $max = null) {
		if (is_int($max)) {
			$stream = @file_get_contents($url, null, null, -1 , $max + 1);
		} else {
			$stream = @file_get_contents($url);
		}

		$temp_file = tempnam(null, 'LRL');
		file_put_contents($temp_file, $stream);

		$path = pathinfo($url);
		$this->file['name'] = $path['basename'];
		$this->file['tmp'] = $temp_file;
	}

	function save($path) {
		if (rename($this->file['tmp'], $path)) {
			return true;
		}

		return false;
	}

	function name() {
		return $this->file['name'];
	}

	function size() {
		return filesize($this->file['tmp']);
	}

	function __destruct() {
		if (file_exists($this->file['tmp'])) {
			@unlink($this->file['tmp']);
		}
	}
}

/**
 * Handle file uploads via XMLHttpRequest
 */
class GetXHR {
	/**
	 * Save the file to the specified path
	 * @return boolean TRUE on success
	 */

	private $file;

	function __construct() {
		if (isset($_SERVER['HTTP_X_FILE_NAME'])) {
			$this->file = $_SERVER['HTTP_X_FILE_NAME'];
		}
	}

	function save($path) {
		$input = fopen("php://input", "r");
		$temp = tmpfile();
		$realSize = stream_copy_to_stream($input, $temp);
		fclose($input);

		if ($realSize != $this->size()) {
			return false;
		}

		$target = fopen($path, "w");
		fseek($temp, 0, SEEK_SET);
		stream_copy_to_stream($temp, $target);
		fclose($target);

		return true;
	}

	function name() {
		return $this->file;
	}

	function size() {
		if (isset($_SERVER['CONTENT_LENGTH'])){
			return (int)$_SERVER['CONTENT_LENGTH'];
		} else {
			throw new Exception('Getting content length is not supported.');
		}
	}
}

/**
 * Handle file uploads via regular form post (uses the $_FILES array)
 */
class GetForm {
	/**
	 * Save the file to the specified path
	 * @return boolean TRUE on success
	 */
	private $_files;
	private $field;
	private $file;

	function __construct() {
		$this->_files = $_FILES;
		$this->field = key($_FILES);
		$this->file = $this->_files[$this->field];
	}

	function set_field($name) {
		if (array_key_exists($name, $this->_files)) {
			$this->field = $name;
			$this->file = $this->_files[$this->field];
		}
	}

	function set_file($key) {
		if (is_array($this->_files[$this->field]) && array_key_exists($key, $this->_files[$this->field]['tmp_name'])) {

			$this->file = array();

			foreach ($this->_files[$this->field] as $attr => $val) {
				$this->file[$attr] = $val[$key];
			}
		}
	}

	function save($path) {
		if(!move_uploaded_file($this->file['tmp_name'], $path)){
			return false;
		}
		return true;
	}

	function name() {
		return $this->file['name'];
	}
	function size() {
		return $this->file['size'];
	}
}

class Uploader {
	private $file;

	private $conf = array(
		'allowed' => array(),
		'size' => 2097152,
		'dir' => '', // Directory in relation to SITE_UPDIR
		'replace' => false,
		'prefix' => '',
		'suffix' => '',
		'random' => false, // Give file a random name
		'filename' => null // Give file a specific name
	);

	function __construct($conf = null, $url = null) {
		$this->conf = array_merge($this->conf, $this->parseConf($conf));

		if (isset($url)) {
			$this->file = new GetURL($url, $this->conf['size']);
			$this->method = 'url';
		} elseif (isset($_SERVER['HTTP_X_FILE_NAME'])) {
			$this->file = new GetXHR();
			$this->method = 'xhr';
		// } elseif ($_FILES && $_FILES[key($_FILES)]['tmp_name']) {
		} elseif ($_FILES) {
			$this->file = new GetForm();
			$this->method = 'form';
		} else {
			$this->file = false;
		}
	}

	private function parseConf($conf) {
		if (isset($conf['allowed'])) {
			if (!is_array($conf['allowed'])) {
				$conf['allowed'] = explode(',', $conf['allowed']);
			}

			$conf['allowed'] = array_map("strtolower", $conf['allowed']);
			$conf['allowed'] = array_map("trim", $conf['allowed']);
		}

		if (isset($conf['size'])) {
			$conf['size'] = fn::bytes($conf['size']);
		}

		return (array) $conf;
	}

	function method() {
		if (isset($this->method)) return $this->method;
		return false;
	}

	function process($conf = null) {
		$conf = is_array($conf) ? array_merge($this->conf, $this->parseConf($conf)) : $this->conf; // Allow global conf override

		try {
			$size = $this->file->size();
		} catch (Exception $e) {
			return array(false, $e->getMessage());
		}

		if ($size == 0) {
			return array(false, 'File is empty');
		}

		if ($size > $conf['size']) {
			return array(false, 'File is too large. Maximum: ' . f::nice_size($conf['size']));
		}

		$pathinfo = pathinfo($this->file->name());
		$filename = $pathinfo['filename'];
		$ext = strtolower($pathinfo['extension']);

		if ($conf['allowed'] && !in_array($ext, $conf['allowed'])) {
			$allowed = implode(', ', $conf['allowed']);
			return array(false, 'Sorry \'.' . $ext . '\' files are not allowed.<br /> Try one of the following: ' . $allowed . '.');
		}

		if ($conf['random']) {
			$filename = fn::random();
		} else if (!empty($conf['filename'])) {
			$filename = trim($conf['filename']);
		} else {
			$filename = fn::slug($filename, '_', true, "\.");
		}

		$up_path = SITE_UPDIR . $conf['dir'];

		if (!$conf['replace'] && file_exists($up_path . $filename . '.' . $ext)) {
			$i = 1;
			while (file_exists($up_path . $filename . $i . '.' . $ext)) {
				$i++;
			}
			$filename = $filename . $i;
			$iteration = $i;
		}

		$filename = $conf['prefix'] . $filename . $conf['suffix'];

		$file = SITE_UPDIR . $conf['dir'] . $filename . '.' . $ext;

		if ($this->file->save($file)){

			$type = $ext;

			if (class_exists('finfo')) {
				$finfo = new finfo;
				$type = $finfo->file($file, FILEINFO_MIME_TYPE);
			}

			$return = array(true, 'file' => array(
					'path' => $file,
					'file' => $conf['dir'] . $filename . '.' . $ext,
					'filename' => $filename . '.' . $ext,
					'original_filename' => $pathinfo['filename'] . '.' . $ext,
					'size' => $size,
					'type' => $type
				));

			if (isset($iteration)) {
				$return['file']['iteration'] = $iteration;
			}

			return $return;
		} else {
			return array(false, 'File could not be saved');
		}
	}

	/**
	 * Returns array('success'=>true) or array('error'=>'error message')
	 */
	function upload($name = null, $conf = null){
		if (!$this->file){
			return array(false, 'No files were uploaded.');
		}

		if ($conf !== null && !is_array($conf)) {
			$conf = null;
		}

		$up_path = SITE_UPDIR . $this->conf['dir'];

		if (!file_exists($up_path) && !mkdir($up_path, 0755, true)) {
			return array(false, 'Uploads directory does not exist and could not be created: ' . $up_path);
		}

		if (!is_writable($up_path)) {
			return array(false, 'Uploads directory is not writable: ' . $up_path);
		}

		if ($name !== null) {
			$this->file->set_field($name);
		}

		if (is_array($this->file->name())) {
			$files = array();

			foreach ($this->file->name() as $key => $file) {
				$this->file->set_file($key);
				$files[] = $this->process($conf);
			}

			return $files;
		} else {
			return $this->process($conf);
		}

	}

	function s3($files, $overwrite = false) {
		if (array_values($files) !== $files) $files = array($files);

		require_once(SITE_PATH . 'includes/lib/aws/sdk.class.php');

		$s3 = new AmazonS3();
		$return = array();

		foreach ($files as $k=>$file) {
			$path = $file['path'] . $file['file'];
			$filepath = isset($file['fullpath']) ? $file['fullpath'] : SITE_UPDIR . $path;

			if (!$overwrite && $s3->if_object_exists('autodoor', $path)) {
				$return[] = array("status" => false, "reason" => 'exists', 'url' => urldecode($s3->get_object_url('autodoor', $path)));
			} else {
				$array = array(
					"body" => file_get_contents($filepath),
					"contentType" => $file['mime'],
					"acl" => $s3::ACL_PUBLIC,
					"storage" => $s3::STORAGE_REDUCED,
					"headers" => array(
						"Expires" => date('D, d M Y H:i:s \G\M\T', strtotime('+10 Years'))
					)
				);

				$obj = $s3->create_object('autodoor', $path, $array);

				if ($obj->status = (int) 200) {
					$return[] = array("status" => true, 'url' => urldecode($s3->get_object_url('autodoor', $path)));
				} else {
					$return[] = array("status" => false, "reason" => $obj->status);
				}
			}
		}

		return $return;
	}
}

/*
define('SITE_UPDIR', 'uploads/');

$args = array(
	'allowed' => array('jpeg', 'jpg', 'png'),
	'size' => '2mb',
	// 'dir' => date('Y') . '/' . date('m') . '/'
);

$url = @$_POST['url'] ? $_POST['url'] : null;

$uploader = new Uploader($args, $url);
$result = $uploader->upload();

switch ($uploader->method()) {
	case 'xhr':
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
		break;
	case 'form':
		break;
	case 'url':
		echo "<pre>";
		print_r($result);
		break;
}

*/
?>
