<?php
require('uploader.php');

define('SITE_PATH', '');
define('SITE_UPDIR', 'uploads/');

$args = array(
	'allowed' => array('jpeg', 'jpg', 'png'),
	'size' => '2mb',
	'dir' => date('Y') . '/' . date('m') . '/'
);

$url = @$_POST['url'] ? $_POST['url'] : null;

$uploader = new Uploader($args, $url);
$result = $uploader->upload();

print_r($result);
?>

<form id="example" method="POST" enctype="multipart/form-data">
	<input type="file" name="file" />
	<input type="submit" name="submit" />
</form>
